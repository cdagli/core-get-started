const APP_NAME = require("../common/index").APP_NAME;
const MEETUP_API_KEY = require("../common/index").MEETUP_API_KEY;
const QLIK_CONFIG = require("../common/index").QLIK_CONFIG;
const HOUR_IN_MILLISECONDS = require("../common/index").HOUR_IN_MILLISECONDS;

const Halyard = require("halyard.js");
const Enigma = require("enigma.js");
const MeetupApi = require("meetup-api");

const meetup = MeetupApi({
    key: MEETUP_API_KEY
});

async function loadData()
{
    console.log("Loading data using Halyard");

    try {
        var response = await getUpcomingEvents();
    } catch(e)
    {
        throw new Error("request failed, details: ", e);
    }
    
    const upcomingEvents = response.events
        .filter(datum => !!datum.venue)
        .map(datum => {
        return {
            eventName: datum.name,
            description: datum.description,
            link: datum.link, 
            localDate: datum.local_data, 
            localTime: datum.local_time, 
            count: datum.yes_rsvp_count, 
            lat: datum.venue.lat, 
            lon: datum.venue.lon, 
            venueName: datum.venue.name, 
            groupName: datum.group.name, 
        }
    });

    console.log(`Fetched and filtered ${upcomingEvents.length} events in total.`);

    const halyard = new Halyard();
    const tableUpcomingEvents = new Halyard.Table(upcomingEvents, "upcomingEvents");
    halyard.addTable(tableUpcomingEvents);

    const session = await Enigma.create(QLIK_CONFIG);
    const qix = await session.open();
    qix.reloadAppUsingHalyard(APP_NAME, halyard, true);
    console.log("Reload complete");
}

async function getUpcomingEvents()
{
    return new Promise((resolve, reject) =>
    {
        meetup.getUpcomingEvents({}, function (error, data) {
            if(error){
                throw error;
            }
            resolve(data);
        });
    })
}

(async () =>
{
    loadData();
    setInterval(loadData, HOUR_IN_MILLISECONDS);
})()
