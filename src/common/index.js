const enigmaMixin = require('halyard.js/dist/halyard-enigma-mixin');
const qixSchema = require('enigma.js/schemas/3.2.json');
const WebSocket = require('ws');

const SECONDS_IN_MILLISECONDS = 1000; 
const MINUTE_IN_MILLISECONDS = 60 * SECONDS_IN_MILLISECONDS;
module.exports.HOUR_IN_MILLISECONDS = MINUTE_IN_MILLISECONDS * 60;

module.exports.QLIK_CONFIG = {
    schema: qixSchema,
    mixins: enigmaMixin,
    url: `ws://http://51.136.21.56:19076/app`,
    createSocket: process.env.IS_NODE_ENV ? url => new WebSocket(url): null
}

module.exports.MEETUP_API_KEY = "666646a21b263b21592343b6d14";

module.exports.APP_NAME = "meetup-app";
