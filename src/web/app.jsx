/* eslint-env browser */

import { getCountryData, getData } from "./utilities";
import * as React from "react";
import ReactDOM from "react-dom";

import {
    ComposableMap,
    ZoomableGroup,
    Geographies,
    Geography,
    Markers,
    Marker
} from "react-simple-maps"

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            markers: []
        }
    }

    async componentDidMount() {
        const data = await getData();
        const markers = this.generateMarkersFromData(data)
        this.setState({ markers });
    }

    render() {
        return (
            <div>
                <h1 className="header">Athlete Counts by Countries in the last 120 years</h1>
                <ComposableMap
                    width="1200"
                    height="1200"
                    projectionConfig={{
                        scale: 200,
                        xOffset: 0,
                        yOffset: -75,
                    }}>
                    <ZoomableGroup zoom={1.2} disablePanning>
                        <Geographies geography={"resources/world-50m.json"}>
                            {(geographies, projection) => geographies.map((geography, idx) => (
                                <Geography
                                    key={idx}
                                    geography={geography}
                                    projection={projection}
                                    style={{
                                        default: {
                                            fill: "#EEE",
                                            stroke: "#000",
                                            strokeWidth: 0.1,
                                            outline: "none",
                                        },
                                    }}
                                />
                            ))}
                        </Geographies>
                        <Markers>
                            {this.state.markers.map((marker, i) => (
                                <Marker
                                    key={i}
                                    marker={marker}
                                    style={{
                                        default: { fill: "#FF5722" },
                                        hover: { fill: "#FFFFFF" },
                                        pressed: { fill: "#FF5722" },
                                    }}
                                >
                                    <circle
                                        cx={0}
                                        cy={0}
                                        r={10}
                                        style={{
                                            stroke: "#FF5722",
                                            strokeWidth: 3,
                                            opacity: marker.count / 1000,
                                        }}
                                    />
                                    <text
                                        textAnchor="middle"
                                        y={marker.markerOffset}
                                        style={{
                                            fontFamily: "Roboto, sans-serif",
                                            fill: "#DDD",
                                        }}
                                    >
                                        {`${marker.name} / ${marker.count}`}
                                    </text>
                                </Marker>
                            ))}
                        </Markers>
                    </ZoomableGroup>
                </ComposableMap>
            </div>
        )
    }

    generateMarkersFromData(data) {
        let countryCode2Coordinates = {};
        data.countryDataLayout.qHyperCube.qDataPages[0].qMatrix.forEach(datum => {
            countryCode2Coordinates[datum[0].qText] = {
                name: datum[3].qText,
                lat: datum[1].qText,
                lon: datum[2].qText
            }
        });

        let athletesCountCoordinates = data.athletesDataLayout.qHyperCube.qDataPages[0].qMatrix.map(datum => {
            return {
                count: datum[1].qText,
                coordinates: countryCode2Coordinates[datum[0].qText] ? [countryCode2Coordinates[datum[0].qText].lon, countryCode2Coordinates[datum[0].qText].lat] : [0, 0],
                name: countryCode2Coordinates[datum[0].qText] ? countryCode2Coordinates[datum[0].qText].name : "NONAME",
                markerOffset: -15
            }
        });

        return athletesCountCoordinates;
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);