import enigma from 'enigma.js';
import Halyard from "halyard.js";
import { APP_NAME, QLIK_CONFIG } from "../common/index";

export async function getData() {
    let dataFilePath;

    const countryDataProperties = {
        qInfo: {
          qType: 'visualization',
          qId: '',
        },
        type: 'my-info-table',
        labels: true,
        qHyperCubeDef: {
          qDimensions: [{
            qDef: {
              qFieldDefs: ['Alpha-3 code'],
            },
          },
          {
            qDef: {
              qFieldDefs: ['Latitude (average)'],
            },
          },
          {
            qDef: {
              qFieldDefs: ['Longitude (average)'],
            },
          },
          {
            qDef: {
              qFieldDefs: ['Country'],
            },
          }
          ],
          qInitialDataFetch: [{
            qTop: 0, qHeight: 200, qLeft: 0, qWidth: 50,
          }],
          qSuppressZero: false,
          qSuppressMissing: true,
        }
    }
    
    const athletesDataProperties = {
        qInfo: {
            qType: 'visualization',
            qId: '',
        },
        type: 'world-athlete-events',
        labels: true,
        qHyperCubeDef: {
            qDimensions: [{
                qDef: {
                    qFieldDefs: ['NOC'],
                    qSortCriterias: [{
                        qSortByAscii: 1,
                    }],
                }
            }],
            qMeasures: [{
                qDef: {
                    qDef: 'Count([NOC])',
                    qLabel: 'Athlete Counts by NOC',
                },
                qSortBy: {
                    qSortByNumeric: -1,
                },
            },
            ],
            qInitialDataFetch: [{
                qTop: 0, qHeight: 500, qLeft: 0, qWidth: 3,
            }],
            qSuppressZero: false,
            qSuppressMissing: false,
        },
    };


    const halyard = new Halyard();
    dataFilePath = '/data/athlete_events.csv';
    const tableAthleteEvents = new Halyard.Table(dataFilePath, {
        name: 'Athlete_events',
        delimiter: ',',
    });
    halyard.addTable(tableAthleteEvents);

    dataFilePath = '/data/country_code_coordinates.csv';
    const tableCountryCodes = new Halyard.Table(dataFilePath, {
        name: 'Country_code_coordinates',
        delimiter: ',',
    });
    halyard.addTable(tableCountryCodes);

    const session = await enigma.create(QLIK_CONFIG);

    const qix = await session.open();

    const app = await qix.createSessionAppUsingHalyard(halyard);
    await app.getAppLayout();

    const athletesDataModel = await app.createSessionObject(athletesDataProperties);
    const countryDataModel = await app.createSessionObject(countryDataProperties);

    return {
        countryDataLayout: await countryDataModel.getLayout(),
        athletesDataLayout: await athletesDataModel.getLayout()
    }
}
