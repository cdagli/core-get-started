const WebSocket = require('ws');
const enigma = require('enigma.js');
const schema = require('enigma.js/schemas/3.2.json');
const mixins = require('halyard.js/dist/halyard-enigma-mixin');

(async () => 
{
  const eventDataProperties = {
      qInfo: {
        qType: 'visualization',
        qId: '',
      },
      type: 'my-info-table',
      labels: true,
      qHyperCubeDef: {
        qDimensions: [{
          qDef: {
            qFieldDefs: ['name'],
          },
        },
        {
          qDef: {
            qFieldDefs: ['group'],
          },
        },
        {
          qDef: {
            qFieldDefs: ['city'],
          },
        },
        {
          qDef: {
            qFieldDefs: ['country'],
          },
        },
        ],
        qInitialDataFetch: [{
          qTop: 0, qHeight: 100, qLeft: 0, qWidth: 50,
        }],
        qSuppressZero: false,
        qSuppressMissing: true,
      },
    };

  const session = enigma.create({
    schema,
    mixins,
    url: 'ws://localhost:19076/app3',
    createSocket: url => new WebSocket(url),
  });
  
  const qix = await session.open();
  const doc = await qix.openDoc('eventApp');
  const model = await doc.createSessionObject(eventDataProperties);
  model.on('changed', async () => {

    const layout = await model.getLayout();
    const matrixLength = layout.qHyperCube.qDataPages[0].qMatrix.length;
    console.log(matrixLength)
    console.log(layout.qHyperCube.qDataPages[0].qMatrix[0][0])
  })
  
  // session.close();
})()

