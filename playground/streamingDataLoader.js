const assert = require('assert');
const WebSocket = require('ws');
const enigma = require('enigma.js');
const schema = require('enigma.js/schemas/3.2.json');
const Halyard = require('halyard.js');
const mixins = require('halyard.js/dist/halyard-enigma-mixin');

assert(process.env.MEETUP_KEY, 'MEETUP_KEY variable isn\'t set on enviroment (use \'set \"MEETUP_KEY=key\"\' on Windows)');

const meetup = require('meetup-api')({
    key: process.env.MEETUP_KEY
});

let count = 1;
let events = [];

meetup.getStreamOpenEvents({}).on('data', function (obj) {
        console.log('%s - %s (%s) at %s (%s)',
            count,
            obj.name,
            obj.group.name,
            obj.group.city,
            obj.group.country.toUpperCase()
        );
    events.push({
        name: obj.name, 
        group: obj.group.name,
        city: obj.group.city,
        country: obj.group.country.toUpperCase()
    });
});

setInterval(reloadData, 1000);

async function reloadData()
{
    try {
        if(events.length)
        {
            console.log("Reloading data");
            const halyard = new Halyard();
            const eventsTable = new Halyard.Table(events, 
                {
                    name: 'Events',
                    fields: [
                        { src: 'name', name: 'name' },
                        { src: 'group', name: 'group' },
                        { src: 'city', name: 'city' },
                        { src: 'country', name: 'country' }
                    ],
                });

            halyard.addTable(eventsTable);

            const session = enigma.create({
                schema,
                mixins,
                url: 'ws://localhost:19076/app3',
                createSocket: url => new WebSocket(url),
            });
            const qix = await session.open();
            const app = await qix.reloadAppUsingHalyard('eventApp', halyard, true);
        }
    } catch (err) {
        console.log('Whoops! An error occurred.', err);
        process.exit(1);
    }
}
